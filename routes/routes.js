const contacts = require("../src/contacts.js");
const jwt = require('express-jwt');
const authCheck = jwt({
    secret: 'NOLjcvxLAj_j6G0NANwiT92l14dHK1rVHF0zh6LN7Xt-DXpS0TOvLKmhLLB3lOCS',
    audience: 'jpxwQqNYPIkIJiZ2xr841Fpg2jUlQBsE'
});

var appRouter = function(app) {
    // get all contacts
    app.get("/contacts", function(request, response) {
        contacts.getContacts()
            .then ( function(result) {
                response.send(result);
            });
    });

    // get one contact
    app.get("/contacts/:id", authCheck, function(request, response) {
        contacts.getContact(request.params)
            .then ( function(result) {
                response.send(result);
            });
    });

    // add new contact
    app.post("/contacts", authCheck, function(request, response) {
        contacts.addContact(request.body)
            .then( function(result) {
                response.send(result);
            });
    });

    // update contact
    app.put("/contacts/:id", authCheck, function(request, response) {
        contacts.updateContact(request.params, request.body)
            .then( function(result) {
                response.send(result);
            });
    });

    // delete contact
    app.delete("/contacts/:id", authCheck, function(request, response) {
        contacts.deleteContact(request.params)
            .then( function(result) {
                response.send(result);
            });
    });
};
 
module.exports = appRouter;
