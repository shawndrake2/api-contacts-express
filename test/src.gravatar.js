var assert = require('assert');
var gravatar = require('../src/gravatar');

describe('gravatar.js', function() {
    describe('#getGravatarImage()', function() {
        it('should return valid gravatar image string', function() {
            const expected = '//www.gravatar.com/avatar/16d86f132910b8368636bf625f232f37';
            assert.equal(gravatar('testemail@email.com'), expected);
        });
    })
});