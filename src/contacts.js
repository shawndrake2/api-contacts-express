var cassandra = require('cassandra-driver');
var cassConfig = require('config.json') ('./config/config.json', process.env.NODE_ENV).cassandra;
var client = new cassandra.Client(cassConfig);
var getGravatarImage = require('./gravatar');

module.exports.getContacts = function () {
    return new Promise( function ( resolve, reject ) {
        const query = 'select * from contacts.contacts';
        client.execute(query, function (error, result) {
            if (error) {
                reject(error);
            }
            resolve(result.rows.sort(function(a,b) {
                var nameA = a.name.toUpperCase();
                var nameB = b.name.toUpperCase();
                if (nameA < nameB){
                    return -1;
                }
                if (nameA > nameB){
                    return 1;
                }
                return 0;
            }));
        });
    });
};

module.exports.getContact = function (params) {
    return new Promise( function ( resolve, reject ) {
        var id = params.id;
        const query = 'select * from contacts.contacts where id = ?';
        client.execute(query, [id], function (error, result) {
            if (error) {
                reject(error);
            }
            let contact = result.rows[0];
            contact.image = getGravatarImage(contact.email);
            resolve(contact);
        });
    });
};

module.exports.addContact = function (params) {
    return new Promise( function ( resolve, reject ) {
        const query = 'insert into contacts.contacts (id, name, email, phone) values (uuid(), ?, ?, ?)';
        client.execute(query, [params.name, params.email, params.phone], function (error, result) {
            if (error) {
                reject(error);
            }
            resolve('{ "status": "success" }');
        });
    });
};

module.exports.updateContact = function (params, values) {
    return new Promise( function ( resolve, reject ) {
        const query = 'update contacts.contacts set name = ?, email = ?, phone = ? where id = ?';
        client.execute(query, [values.name, values.email, values.phone, params.id], function (error, result) {
            if (error) {
                reject(error);
            }
            resolve('{ "status": "success" }');
        });
    });
};

module.exports.deleteContact = function (params) {
    return new Promise( function ( resolve, reject ) {
        const query = 'delete from contacts.contacts where id = ?';
        client.execute(query, [params.id], function (error, result) {
            if (error) {
                reject(error);
            }
            resolve('{ "status": "success" }');
        });
    });
};
