var crypto = require("crypto");

const getGravatarImage = function (email) {
    const baseUrl = "//www.gravatar.com/avatar/";
    const hash = md5(email);
    return (baseUrl + hash).trim();
};

function md5(str) {
    str = str.toLowerCase().trim();
    let hash = crypto.createHash("md5");
    hash.update(str);
    return hash.digest("hex");
}

module.exports = getGravatarImage;